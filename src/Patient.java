
import java.util.List;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author SHUBHAM PADHYA
 */
public class Patient {
   private String name;
    private String dateofbirth;
    private String familydoctor;
    private List<Wristband> wristband; 

    
    
    public Patient(String name, String dateofbirth, String familydoctor, List <Wristband> wristband){
        
        this.name=name;
        this.dateofbirth=dateofbirth;
        this.familydoctor=familydoctor;
        
    }
    
    public String getName(){
     return name;   
    }
    
    public String getDateofbirth(){
     return dateofbirth;   
    }
    
    public String Familydoctor(){
     return familydoctor;   
    }
    
    public List<Wristband> getWristband(){
     return wristband;   
    }
    
    public void setName(String name){
        this.name=name;
    }
    
    public void setDateofbirth(String dateofbirth){
        this.name=dateofbirth;
    }
    
    public void setFamilydoctor(String familydoctor){
        
        this.familydoctor=familydoctor;
    }
    
    public void setWristband(List<Wristband> wristband){
        this.wristband=wristband;
    }
    
    }
